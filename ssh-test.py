import os
import argparse
import paramiko

#latest version of both OpenSSH and LibreSSL
def test_ssh_host(host):
    '''
    used to test ssh host
    '''
    try:
        key = paramiko.RSAKey.from_private_key_file("~/.ssh/developer_key")
        client = paramiko.SShClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(hostname = host,username = "developer", pkey = key, timeout=30)
        print("connected to client " + host)
        print("Running command sshd -V")
        stdin, stdout, stderr = client.exec_command("sshd -V")
        #dont check stderr as sshd -V gives a return code of 127 if you echo $?
        #get the second line of the output to get the version
        stdout = stdout.read().split("\n")[1].split(",")
        #this version should probably be made configurable or be read from somewhere
        #was thinking if I could set it based on one of the version of the host but then
        #would have a tough time differentiating what is the correct version
        if(stdout[0] != "OpenSSH_7.8p1" or stdout[0] != "OpenSSH_7.8p1"):
            raise Exception("version of OpenSSH is old")
        if(stdout[0] != "LibreSSL 2.9.1"):
            raise Exception("version of LibreSSL is too old")
    except paramiko.BadHostKeyException as e:
        #reporting exception
        print("Bad host key provided")
        print("Error is " + str(e))
        return(False)
    except paramiko.AuthenticationException as e:
        #reporting exception
        print("Unable to authenticate properly")
        print("Error is " + str(e))
        return(False)
    except paramiko.SSHException as e:
        #reporting exception
        print("Unable to SSH to host")
        print("Error is " + str(e))
        return(False)
    except Exception as e:
        #reporting exception
        print("Exception occured")
        print("Error is " + str(e))
        return(False)
    return(True)

def main():
    '''
    driver function to run whole program
    '''
    parser = argparse.ArgumentParser(description='keyboard transform')
    parser.add_argument('-f','--file',type=str,help="a path to a file that has the hostnames/ips of nodes to test against. Ex. /root/hosts.txt")
    args = parser.parse_args()
    if(args.file is None):
        print("Please provide a file")
        return(False)
    #check if file with all the hosts exists
    if(os.path.isfile(args.file) == False):
        print("Please provide a file that exists")
        return(False)
    #check if ssh key exists
    if(os.path.isfile("~/.ssh/developer_key") == False):
        print("Please create the ssh key in ~/.ssh/developer_key and copy the key to the hosts")
        return(False)
    hosts_file = open(args.file)
    lines = hosts_file.readlines()
    for line in lines:
        if(test_ssh_host(line) == False):
            print(line + " host needs to be fixed")
        print("-------------------------------------------")
    return(True)

if __name__ == '__main__':
    print(main())