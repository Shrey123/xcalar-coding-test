Prerequisites:
sudo pip install -r requirements.txt
----------------------------------------------------------------------------------------------
python keyboard-transform.py -h
usage: keyboard-transform.py [-h] [-w WORD] [-t TRANSFORM]

keyboard transform

optional arguments:
  -h, --help            show this help message and exit
  -w WORD, --word WORD  a string to test transform
  -t TRANSFORM, --transform TRANSFORM  transform process seperated by spaces. ex. "V V H -1"

----------------------------------------------------------------------------------------------
python ssh-test.py -h
usage: ssh-test.py [-h] [-f FILE]

keyboard transform

optional arguments:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  a path to a file that has the hostnames/ips of nodes
                        to test against. Ex. /root/hosts.txt