import sys
import argparse
import re
#global map
map = {}
#global list
lst = [['1','2','3','4','5','6','7','8','9','0'],
           ['q','w','e','r','t','y','u','i','o','p'],
           ['a','s','d','f','g','h','j','k','l',';'],
           ['z','x','c','v','b','n','m',',','.','/']]
def generate_map():
    '''
    generates map with the key being the letter and value being verticle
    and horizontal
    map -> key = "key on keyboard", value = list with 0 index being horizontal mapping
    and 1 index being vertical mapping
    '''
    global lst
    tmplst = []
    for i in range(len(lst)):
        for j in range(len(lst[i])):
            tmplst.append(lst[i][len(lst[i])-j-1])
            tmplst.append(lst[len(lst)-i-1][j])
            map[lst[i][j]] = tmplst
            tmplst = []

def shift(word,n):
    '''
    shift word by n either in the positive direction or the negative
    direction
    '''
    global lst
    index = 0
    if(n > 40 or n < -40):
        print("Please provide a shift offset that is less than 40 and greater than -40")
        return(False)
    for i in range(len(word)):
        for j in range(len(lst)):
            if(word[i].isupper() and word[i].lower() in lst[j]):
                index = lst[j].index(word[i].lower())+n
                #need to work on handing case where n is a large number like greater than 10
                #if the shift brings you to the next line
                if(index > 9):
                    word[i] = lst[j+1][index-9-1].upper()
                #if the shift brings you to the previous line
                elif(index < 0):
                    word[i] = lst[j-1][9-index+1].upper()
                else:
                    word[i] = lst[j][index].upper()
            elif(word[i] in lst[j]):
                index = lst[j].index(word[i])+n
                #if the shift brings you to the next line
                if(index > 9):
                    if(j+1 > len(lst)-1):
                        word[i] = lst[0][index-9-1]
                    else:
                        word[i] = lst[j+1][index-9-1]
                #if the shift brings you to the previous line
                elif(index < 0):
                    if(j-1 < 0):
                        word[i] = lst[len(lst)-1][10+index]
                    else:
                        word[i] = lst[j-1][10+index]
                else:
                    word[i] = lst[j][index]
    return(word)


def vertical_mirror(word):
    '''
    perform vertical transform on a word
    '''
    global map
    for i in range(len(word)):
        if(word[i].isalpha() and word[i].isupper() and word[i].lower() in map):
            word[i] = map[word[i].lower()][1].upper()
        elif(word[i] in map):
            word[i] = map[word[i]][1]
        else:
            print(word[i] + " is not a valid character")
            return(False)
    return(word)

def horizontal_mirror(word):
    '''
    perform horizontal transform on a word
    '''
    global map
    # print(map)
    for i in range(len(word)):
        if(word[i].isalpha() and word[i].isupper() and word[i].lower() in map):
            word[i] = map[word[i].lower()][0].upper()
        elif(word[i] in map):
            word[i] = map[word[i]][0]
        else:
            print(word[i] + " is not a valid character")
            return(False)
    return(word)
    

def main():
    parser = argparse.ArgumentParser(description='keyboard transform')
    parser.add_argument('-w','--word',type=str,help='a string to test transform')
    parser.add_argument('-t','--transform',type=str,help='transform process seperated by spaces. ex. "V V H -1"')
    args = parser.parse_args()
    if(args.word is None or args.word == ""):
        return("please enter valid input")
    word = args.word
    word = list(word)
    #generate mapping in memory
    generate_map()
    transforms = args.transform.split(" ")
    if(len(transforms) == 0):
        return("please enter valid input for the transform")
    for tranform in transforms:
        if(tranform.upper() == "V"):
            word = vertical_mirror(word)
            if(word == False):
                print("failed at vertical mirror")
                return("please only include valid characters in your word")
            print("word after vertical mirror: " + "".join(word))
            print("-------------------------------------------------------")
        elif(tranform.upper() == "H"):
            word = horizontal_mirror(word)
            if(word == False):
                print("failed at horizontal mirror")
                return("please only include valid characters in your word")
            print("word after horizontal mirror: " + "".join(word))
            print("-------------------------------------------------------")
        elif(re.match("^-?[0-9]+$",tranform)):
            word = shift(word,int(tranform))
            if(word == False):
                print("failed at shift")
                return("please only include valid characters in your word")
            print("word after shift: " + "".join(word))
            print("-------------------------------------------------------")
        else:
            #stops transform once it notices an invalid transform
            return(tranform + " is not a valid transform")
    return("".join(word))
if __name__ == '__main__':
    print(main())